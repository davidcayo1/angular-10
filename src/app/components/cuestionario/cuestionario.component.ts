import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cuestionario',
  templateUrl: './cuestionario.component.html',
  styleUrls: ['./cuestionario.component.scss']
})
export class CuestionarioComponent implements OnInit {
  

  form: FormGroup;
  respuestas!: string;
  advertencia: boolean;

  constructor(fb: FormBuilder) {
    this.advertencia = false;

    this.form = fb.group({
      gender: ['Male', Validators.required],
      pregunta1: ['No mucho', Validators.required],
      pregunta2: ['Rojo', Validators.required],
      pregunta3: ['Negro', Validators.required],
      pregunta4: ['1 Kilo de Plumas', Validators.required],
      pregunta5: ['3 Dedos', Validators.required],
      aceptaCondiciones: ['true', Validators.requiredTrue],
      uno: ['1.- ¿A usted le gusta las peliculas de terror?'],
      dos: ['2.- ¿Qué color de bolígrafo usa?'],
      tres: ['3.- ¿De qué color es el caballo blanco de Napoleon?'],
      cuatro: ['4.- ¿Que és más pesado 1 Kilo de plumas o 1 Kilo de Oro?'],
      cinco: ['5.- ¿Cuántos dedos tiene una gallina en su pata?'],
    });
  }

  ngOnInit(): void {
  }

  save(): void {
    if (this.form.value.aceptaCondiciones === false) {
      // this.form.hasError('required', 'aceptaCondiciones')
      this.advertencia = true;
    
    } else {
      this.advertencia = false;

      this.respuestas = `
        Respuestas:
        ${this.form.value.uno}
        R.- ${this.form.value.pregunta1}
        ${this.form.value.dos}
        R.- ${this.form.value.pregunta2}
        ${this.form.value.tres}
        R.- ${this.form.value.pregunta3}
        ${this.form.value.cuatro}
        R.- ${this.form.value.pregunta4}
        ${this.form.value.cinco}
        R.- ${this.form.value.pregunta5}
      `;

      // seperamos el texto en un array po espacios y luego lo unimos con br para su salto de línea
      this.respuestas = this.respuestas.split("\n").join("<br>")
    }   
   
  }
}
